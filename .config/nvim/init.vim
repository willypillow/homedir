    "echo" "The following is intepreted as a shell script."
    "echo" "It installs Neovim and this config when invoked."

    "set" -x

    "mkdir" -p ~/.bin
    "curl" -L -o ~/.bin/nvim 'https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage'
    "chmod" u+x ~/.bin/nvim

    "mkdir" -p ~/.config/nvim
    "cp" "$0" ~/.config/nvim/init.vim

    "exit"

    " Now the following is interpreted as VimL.
    " This extracts the config from this file and loads them.

    lua
    \  local script = vim.fn.expand("<sfile>");
    \  local vims = {};
    \  local is_codeblock = false;
    \  for line in io.lines(script) do
    \    if string.sub(line, 1, 3) == "```" then
    \      is_codeblock = not is_codeblock
    \    elseif is_codeblock then
    \      vims[#vims + 1] = line
    \    end
    \  end
    \  vim.cmd(table.concat(vims, "\n"))

    finish

# Vimrc #

``` vim
" vim:foldmethod=marker
```

This is a "literate" Neovim `init.vim` that tries to go into detail why my settings are the way they are. This file is a [polyglot](https://en.wikipedia.org/wiki/Polyglot_(computing)) and can be used in three ways:

1. Markdown: Renders the document you are reading right now
2. VimL: Loads the config described in this file. In other words, you can use this file directly as your `init.vim`
3. Shell: Downloads the latest Neovim AppImage to `~/.bin/nvim` and installs this config to `~/.config/nvim/init.vim`

Before we start, here is some information about myself. I am a software developer mainly coding in C++, Python, and Scala, but also dabble in a bit of everything else. Besides "normal" programming, I also do competitive programming in C++ and write prose and take notes in Markdown. I love (Neo)vim because of its:

- extensibility
- ubiquitousness
- low resource usage and high performance
- tight integration with the shell
- [highly composable and elegant language](https://www.youtube.com/watch?v=wlR5gYd6um0)

In my configuration, I try to not change the text editing experience too much. For example, I tend to not overwrite built-in bindings or use plugins such as [EasyMotion](https://github.com/easymotion/vim-easymotion). This allows me to maintain most of my muscle memory even when I am on a remote machine without my `vimrc`.

## Sane Defaults ##

``` vim
" Import sane defaults {{{1
```

Even though Neovim added quite a few sensible defaults compared to Vim ([`|:h vim-differences|`](https://neovim.io/doc/user/vim_diff.html)), there are still a few useful settings that are in Vim's [`defaults.vim`](https://github.com/vim/vim/blob/master/runtime/defaults.vim) but not in Neovim.

``` vim
" Adopted from defaults.vim in vanilla vim {{{2
" Mouse support
set mouse=a
" Jump to last location on startup {{{3
augroup vimStartup
  autocmd!
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
    \ |   exe "normal! g`\""
    \ | endif
augroup END
" }}}3
" Diff with original file {{{3
command! DiffOrig vert new | set bt=nofile | r ++edit # | 0d_
  \ | diffthis | wincmd p | diffthis
" }}}3
" }}}2
```

We also use the mapping from Tim Pope's [`sensible.vim`](https://github.com/tpope/vim-sensible/blob/master/plugin/sensible.vim) to turn off search highlighting when `^L` is pressed.

``` vim
" ^L to turn off search highlighting
nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
```

``` vim
" }}}1
```

## Install Plugins ##

See the [Plugins](#plugins) section for more information.

``` vim
" Install plugins {{{1
lua << EOF
local plugins = {
  -- Colorschemes
  "https://github.com/robertmeta/nofrils",
  "https://github.com/sainnhe/gruvbox-material",
  -- Writing
  -- "https://github.com/fncll/wordnet.vim",
  -- "https://github.com/iamcco/markdown-preview.nvim",
  "https://github.com/vim-pandoc/vim-pandoc",
  "https://github.com/vim-pandoc/vim-pandoc-syntax",
  -- Note-taking
  "https://github.com/alok/notational-fzf-vim",
  "https://github.com/vimwiki/vimwiki",
  -- Editing (Operations and text objects)
  "https://github.com/michaeljsmith/vim-indent-object",
  "https://github.com/tpope/vim-commentary",
  "https://github.com/tpope/vim-surround",
  "https://github.com/tpope/vim-unimpaired",
  "https://github.com/wellle/targets.vim",
  -- Configuration
  "https://github.com/editorconfig/editorconfig-vim",
  "https://github.com/tpope/vim-sleuth",
  -- Language Tooling
  "https://github.com/dense-analysis/ale",
  "https://github.com/neovim/nvim-lspconfig",
  "https://github.com/nvim-treesitter/nvim-treesitter",
  -- Miscellaneous
  -- "https://github.com/dstein64/vim-startuptime",
  "https://github.com/folke/which-key.nvim",
  "https://github.com/junegunn/fzf",
  "https://github.com/junegunn/fzf.vim",
  "https://github.com/mbbill/undotree",
}
local fn = vim.fn
local root_path = fn.stdpath('data')..'/site/pack/plugins/start/'
for _, p in pairs(plugins) do
  local name = ""
  for str in string.gmatch(p, "/([^/]*)$") do
    name = str
  end
  local install_path = root_path .. name
  if fn.empty(fn.glob(install_path)) > 0 then
    print("Installing " .. install_path)
    if name == "vimwiki" then
      fn.system({"git", "clone", "--depth", "1", "--branch", "dev", p, install_path})
    else
      fn.system({"git", "clone", "--depth", "1", p, install_path})
    end
  end
end
EOF
" }}}1
```

## Basic Options ##

``` vim
" Options {{{1
```

We set `fsync` to prevent losing data. See <https://github.com/neovim/neovim/issues/9888> for a discussion on the topic.

``` vim
set fsync
```

Allow using the system clipboards.

``` vim
set clipboard^=unnamedplus,unnamed
```

Relative numbers are sort of my guilty pleasure. People have criticized `relativenumber` since movements such as `3j` do not modify the jump list, making them difficult to undo. However, a lot of the time it feels more intuitive and convenient than other ways of moving around.

``` vim
set number relativenumber
```

Set `lazyredraw` for better macro performance.

``` vim
set lazyredraw
```

Enable `list`, mostly so that one can see trailing spaces and the difference between spaces and tabs more clearly.

``` vim
set list
```

Make various tweaks to the UI to make it less cluttered.

``` vim
set laststatus=2
set shortmess=aIcF
```

Search-related options. I disable `wrapscan` since I sometimes overlook the "reached end of file" message and loop around the file many times.

``` vim
set smartcase ignorecase
set inccommand=split
set nowrapscan
```

Enable `virtualedit` in visual block mode. This is useful for, e.g., manipulating ASCII art.

``` vim
set virtualedit=block
```

Indention settings. Note that I prefer tabs to spaces (specifically, tabs for indention and spaces for alignment) since the end user can choose whatever width she wants it to be. Also, tabs are better for accessibility (e.g., screen readers) as they provide more "semantic meaning" than spaces.

``` vim
set tabstop=2 shiftwidth=2 shiftround
```

Turn on persistent undo. This works well with the UndoTree plugin, which we will introduce later.

``` vim
set undofile
```

Set some external programs. The `fish` shell and `ripgrep` are, among Neovim, `autojump`, `fd`, and `starship`, some of my must-have CLI tools.

``` vim
set grepprg=rg\ --vimgrep\ $* grepformat=%f:%l:%c:%m
set shell=fish
```

Make the spell checker and the `gw` (text wrapping) operation more friendly to Chinese characters.

``` vim
set spelllang=en_us,cjk
set formatoptions+=mM
```

Enable the thesaurus (which can be invoked by `<C-x><C-t>`). The file can be downloaded from <https://www.gutenberg.org/files/3202/files/mthesaur.txt>.

``` vim
" Thesaurus {{{2
lua << EOF
-- Download if it does not exist
local fn = vim.fn
local path = fn.stdpath('data') .. '/words.txt'
if fn.empty(fn.glob(path)) > 0 then
  fn.system({"curl", "https://www.gutenberg.org/files/3202/files/mthesaur.txt", "-o", path})
end
vim.opt.thesaurus = path
EOF
" }}}
```

Merge the number column and sign column (used for, e.g., ALE diagnostics) to save space.

``` vim
set signcolumn=number
```

Enable better `diff` algorithms.

``` vim
" Ignore whitespace
set diffopt+=iwhiteall
" Preserve wrap settings
set diffopt+=followwrap
" Use more advanced algorithms
set diffopt+=algorithm:histogram,indent-heuristic
```

Set the leader and local leader to be keys that are more accessible.

``` vim
let g:mapleader = "\<Space>"
let g:maplocalleader = ","
```

Explicitly set executable paths and disable most providers to hopefully reduce startup time.

``` vim
let g:python3_host_prog = '/usr/bin/python3'
let g:loaded_python_provider = 0
let g:loaded_python3_provider = 0
let g:loaded_ruby_provider = 0
let g:loaded_perl_provider = 0
let g:loaded_node_provider = 0
```

``` vim
" }}}1
```

## Themes ##

``` vim
" Themes {{{1
```

I am a huge proponent of turning off syntax highlighting for a more distraction-free environment. Still, I use the `nofrils` colorscheme so that the comments can "pop out". This allows comments to be placed at a higher importance compared to themes that "fade out" the comments. See [the author's blog post](https://robertmeta.com/posts/syntax-highlighting-off/) for why not having syntax highlighting in general is a great idea.

``` vim
set notermguicolors
let g:nofrils_heavycomments = 1
colorscheme nofrils-dark
```

However, I still find syntax highlighting useful for diffs and occasionally Markdown files. Hence, I have a mapping that toggles between `nofrils` and [`gruvbox_material`](https://github.com/sainnhe/gruvbox-material), a colorscheme with soft colors that I like a lot.

``` vim
" Toggle colorscheme (sometimes I want moar colors for, e.g., diff or md) {{{2
function! ToggleColorscheme()
  if g:colors_name =~ "^nofrils-"
    set termguicolors
    colorscheme gruvbox-material
    edit
  else
    set notermguicolors
    colorscheme nofrils-dark
    edit
  endif
endfunction
nnoremap <silent> <Leader>tc :call ToggleColorscheme() <CR>
" }}}2
```

``` vim
" }}}1
```

## Mappings ##

``` vim
" Mappings {{{1
```

Some simple miscellaneous mappings.

``` vim
" clang-format
noremap <Leader>ff :%!clang-format -style=file <CR>
" Map Ctrl-Backspace to <C-w> for consistency with GUI programs
noremap! <C-h> <C-w>
```

Copying the whole file to clipboard is useful in competitive programming scenarios, where one often has to paste code into an upload form.

``` vim
" Yank whole file
noremap <C-q> :%yank <CR>
```

Hotkey to toggle `colorcolumn`. Useful as a quick-and-dirty way to see if I have lines that are too long.

``` vim
" Toggle colorcolumn=80 {{{2
function! ToggleColorColumn()
  if &colorcolumn ==# ""
    set colorcolumn=80
  else
    set colorcolumn=
  endif
endfunction
nnoremap <silent> <Leader>hc :call ToggleColorColumn() <CR>
" }}}2
```

A shortcut for uploading files to my [Linx](https://github.com/andreimarcu/linx-server) instance, so I can easily share snippets with others.

``` vim
" Pastebin
cabbrev pb !curl -T '%' -H "Linx-Randomize: yes" https://up.nerde.pw/upload/
```

Set mappings to compile/run the current file. This is enabled for C++, Python, and Markdown. In particular, a lot of warnings and sanitizers have been enabled for C++ to make debugging competitive programming solutions easier. For Markdown documents, we use Pandoc to convert the file to HTML and apply the [Water.css](https://watercss.netlify.app/) stylesheet.

``` vim
" Clang++ compile flags {{{2
let clangflags = '
  \ -Weverything
  \ -Wno-c++98-compat-pedantic
  \ -Wno-missing-prototypes
  \ -Wno-missing-variable-declarations
  \ -Wno-sign-conversion
  \ -DDEBUG
  \ -D_GLIBCXX_DEBUG
  \ -D_GLIBCXX_DEBUG_PEDANTIC
  \ -D_GLIBCXX_SANITIZE_VECTOR
  \ -D_GLIBCPP_CONCEPT_CHECKS
  \ -fsanitize=address
  \ -fsanitize-address-use-after-scope
  \ -fsanitize=pointer-compare
  \ -fsanitize=pointer-subtract
  \ -fsanitize=cfi
  \ -fno-sanitize=cfi-icall
  \ -fsanitize=undefined
  \ -fsanitize=implicit-conversion,integer
  \ -fsanitize=nullability,local-bounds
  \ -fsanitize=float-divide-by-zero,float-cast-overflow
  \ -fcf-protection=full
  \ -fstack-protector-all
  \ -fstack-clash-protection
  \ -fPIE
  \ -O0
  \ -march=native
  \ -fopenmp
  \ -flto
  \ -ggdb3
  \ -fvisibility=hidden
  \ -fstandalone-debug
  \ -std=c++17
  \'
" }}}2
```

``` vim
" Compile commands {{{2
augroup compile
  autocmd!
  autocmd filetype c,cpp
    \ let &l:makeprg="clang++
      \" . clangflags . "
      \ -pie
      \ -Wl,-z,relro,-z,now
      \ -Wl,-z,noexecstack
      \ -o a.out
      \ -lmcheck
      \ '%'"
  autocmd filetype c,cpp
    \ nnoremap <buffer> <F4>
    \ :write <Bar>
    \ make <Bar>
    \ :vsplit <Bar> terminal ./a.out <CR>
  autocmd filetype c,cpp
    \ nnoremap <buffer> <F5>
    \ :write <Bar>
    \ make <Bar>
    \ :vsplit <Bar> terminal gdb ./a.out <CR>
  autocmd filetype python
    \ nnoremap <buffer> <F4>
    \ :write <Bar>
    \ vsplit <Bar> terminal python "%" <CR>
  autocmd filetype markdown,mkd,vimwiki
    \ nnoremap <buffer> <F4>
    \ :write <Bar>
    \ !pandoc
      \ --standalone
      \ --tab-stop=2
      \ --css='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.min.css'
      \ --mathjax='https://cdn.jsdelivr.net/npm/mathjax@latest/es5/tex-mml-chtml.js'
      \ -o ~/Downloads/out.html
      \ "%" <CR><CR>
  autocmd filetype markdown,mkd,vimwiki
    \ nnoremap <buffer> <F5>
    \ :silent !open ~/Downloads/out.html <CR>
augroup END
" }}}2
```

``` vim
" }}}1
```

## Plugins ##

``` vim
" Plugins {{{1
```

The plugins I use are listed in the section [Install Plugins](#install-plugins). For managing plugins, I do not use any external package managers. Instead, I simply use the built-in package functionality, cloning the repositories to `$HOME/.local/share/nvim/site/pack/plugins/start`.

We will now go through a few of these categories in detail. (A lot of the plugins above are, however, fairly self-explanatory and do not require any configuration, and will thus be omitted.)

### Writing and Note-taking ###

I do a lot of writing in Vim with Markdown, including school assignments, documentation, and notes. Papers and documents are usually converted to PDF or HTML with Pandoc. At the same time, I use Vimwiki and Git to manage notes, pushing the repository to a remote server running [Gitit](https://github.com/jgm/gitit) for web access. The notes are also synced to a [Nextcloud](https://nextcloud.com/) instance to allow quick editing from the Nextcloud Notes app on my phone.

More information about my note-taking workflow can be found at [Note-taking](Note-taking).

For showing the outline of a document (`:TOC`), section folding, and other Markdown syntax stuff, I use `vim-pandoc` and `vim-pandoc-syntax`. Another benefit of `vim-pandoc` is its "conceal" functionality that hides formatting characters for the lines your cursor is not on. Besides the default element it hides, I also prefer concealing hyperlinks so that the UI looks even cleaner.

``` vim
" Vim-pandoc-syntax {{{2
let g:pandoc#syntax#conceal#urls = 1
" }}}2
```

Basic Vimwiki settings. The `links_space_char` variable replaces spaces in note filenames with underscores. This is simply so that I can emulate backlink functionality in Gitit by searching for the filename. (In Gitit, if the search term includes spaces, it would be treated as *multiple* terms ORed together.)

``` vim
" Vimwiki {{{2
let g:vimwiki_list =
  \ [{ 'path': '~/sync/vimwiki/',
  \    'syntax': 'markdown',
  \    'ext': '.md',
  \    'links_space_char': '_' }]
let g:vimwiki_ext2syntax = { '.md': 'markdown' }
let g:vimwiki_filetypes = ['markdown', 'pandoc']
```

I had to remap some Vimwiki default binding since they do not work on my terminal (Konsole). In the case of `VimwikiToggleListItem` (`<C-Space>`), it conflicts with my shortcuts for IME switching.

``` vim
augroup vimwiki_syntax
  autocmd!
  autocmd filetype vimwiki* nmap <Leader>wtt <Plug>VimwikiToggleListItem
  autocmd filetype vimwiki* nmap <Leader>wvs <Plug>VimwikiVSplitLink
  autocmd filetype vimwiki* nmap <Leader>wsp <Plug>VimwikiSplitLink
augroup END
```

The default `:VimwikiBacklinks` command does not provide previews of the contexts of the incoming links. The following binding fixes this issue.

``` vim
function! BacklinksFillPreview()
  let loclist = getloclist(0)
  let new_locations = []
  for loc in loclist
    let file = bufname(loc.bufnr)
    call add(new_locations,
      \ extend(loc, {'text': readfile(file)[loc.lnum - 1]}))
  endfor
  call setloclist(0, new_locations, 'r')
endfunction
nnoremap <Leader>wb
  \ :execute 'VWB' <Bar> call BacklinksFillPreview() <Bar> Loc <CR>
" }}}2
```

The plugin `notational-fzf-vim` allows me to easily search in my notes with FZF. In the case that the searched term does not exist, it automatically creates a page with the corresponding title, similar to [Notational Velocity](https://notational.net/).

``` vim
" Notational-fzf-vim {{{2
let g:nv_search_paths = ["~/sync/vimwiki"]
nnoremap <Leader>nv :NV <CR>
" }}}2
```

Shortcut for live Markdown previews. Normally I find live previews distracting, but for large $\LaTeX$ math formulas where it is easy to make mistakes, having previews is still valuable.

``` vim
" markdown-preview.nvim {{{2
nnoremap <Leader>mp :MarkdownPreview <CR>
" }}}2
```

Bindings to search for the word under the cursor in WordNet. This is useful for finding synonyms, essentially acting as another thesaurus when writing.

``` vim
" Wordnet.vim {{{2
noremap <Leader>wnd :call wordnet#overviews("<C-r>=expand("<cword>")<CR>") <CR>
noremap <Leader>wns :call wordnet#synonyms("<C-r>=expand("<cword>")<CR>") <CR>
noremap <Leader>wnb :call wordnet#browse("<C-r>=expand("<cword>")<CR>") <CR>
" }}}2
```

### Language Tooling ###

To reduce distractions, ALE linting is only triggered when the file is written.

Also note that `--ignore-missing-imports` is passed to `mypy` to reduce noise when using external libraries.

``` vim
" ALE {{{2
let g:ale_enabled = 0
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 0
let g:ale_cpp_cppcheck_options = '
  \ --enable=all
  \ --inconclusive
  \ --inline-suppr'
let g:ale_cpp_cpplint_options = '--root . --filter=
  \-build/c++11,
  \-legal/copyright,
  \-runtime/references,
  \-whitespace/newline,
  \-whitespace/tab,
  \-readability/nolint,
  \-readability/casting,
  \-runtime/explicit,
  \-runtime/int,
  \-runtime/string,
  \-whitespace/braces,
  \-whitespace/comments,
  \-build/namespaces'
let g:ale_python_mypy_options = '--ignore-missing-imports'
" }}}2
```

My configuration for `nvim-treesitter` is fairly bare-bones. It is mostly identical to the official examples, with all the built-in modules enabled.

``` vim
" nvim-treesitter {{{2
lua << EOF
require("nvim-treesitter.configs").setup {
  ensure_installed = "all",
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = true,
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "gnn",
      node_incremental = "grn",
      scope_incremental = "grc",
      node_decremental = "grm",
    },
  },
  indent = {
    enable = true,
  },
}
EOF
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
" }}}2
```

I mostly use the LSP feature with `nvim-lspconfig` to invoke [ltex-ls](https://github.com/valentjn/ltex-ls) for grammar checking when writing prose. The key bindings are pretty much the same as those in the official configuration examples, but with `<Space>` changed to `<LocalLeader>` to avoid collisions.

A large part of the configuration below is to enable (almost) all the rules in LanguageTool. Unfortunately, I have yet to find a more elegant way of doing so.

Also, we need to overwrite `language_id_mapping` so that it properly detects Vimwiki files as Markdown.

``` vim
" nvim-lspconfig {{{2
lua << EOF
-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { noremap = true, silent = true }
vim.api.nvim_set_keymap('n', '<LocalLeader>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
vim.api.nvim_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
vim.api.nvim_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
vim.api.nvim_set_keymap('n', '<LocalLeader>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<LocalLeader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<LocalLeader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<LocalLeader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<LocalLeader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<LocalLeader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<LocalLeader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<LocalLeader>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
end
require("lspconfig").ltex.setup {
  on_attach = on_attach,
  settings = {
    ltex = {
      enabledRules = { -- {{{
        ["en-US"] = { "ABUNDANCE", "ACCEDE_TO", "ACCELERATE", "ACCENTUATE", "ACCOMMODATION", "ACCOMPANY", "ACCOMPLISH", "ACCRUE", "ACCURATE", "ACQUIRE", "ADVERB_WORD_ORDER_10_TEMP", "AGREEMENT_THEIR_HIS", "ALL_THINGS_CONSIDERED", "ALONG_THE_LINES_OF", "ALSO_SENT_END", "And", "AND_ALSO", "APOSTROPHE_PLURAL_", "ARE_ABLE_TO", "ARTICLE_MISSING", "ARTICLE_UNNECESSARY", "AS_A_MATTER_OF_FACT", "AS_PER", "ASSIST_ASSISTANCE", "AT_ALL_TIMES", "AT_YOUR_EARLIEST_CONVENIENCE", "BARE_INFINITIVE_CAUSATIVE_VERB_MAKE_PRP_VBZ", "BARE_INFINITIVE_VERB_OF_PERCEPTION_SEE_PRP_VBZ", "BASIS_ON_A", "BE_ADVISED", "BE_A_X_ONE", "BECAUSE", "BELATED", "BEST_EVER", "BLEND_TOGETHER", "BRIEF_MOMENT", "BY_MEANS_OF", "CAN_NOT", "CANT_HELP_BUT", "CHILDISH_LANGUAGE", "COMMA_BEFORE_AND", "COMMA_WHICH", "CURRENTLY", "DESPITE_THE_FACT", "DIVIDE_IN_INTO", "DRAW_ATTENTION", "DT_NNS_AGREEMENT", "DUE_TO_THE_FACT", "EACH_AND_EVERY_NOUN", "EMPHATIC_REFLEXIVE_PRONOUNS", "EN_CONSISTENT_APOS", "EN_REDUNDANCY_REPLACE", "EN_UNPAIRED_QUOTES", "EXACT_SAME", "FLASHPOINT", "FOR_ALL_INTENTS_AND_PURPOSES", "FOR_THE_MOST_PART", "FOR_THE_PURPOSE_OF", "GONNA_TEMP", "GOT_IT_DONE", "HAVE_A_TENDENCY", "HAVE_THE_ABILITY_TO", "HOPEFULLY", "IN_A_MANNER_OF_SPEAKING", "IN_A_VERY_REAL_SENSE", "in_excess_of", "IN_MY_OPINION", "IN_ORDER_TO", "IN_TERMS_OF", "IN_THE_AFFIRMATIVE", "IN_THE_CASE_OF", "IN_THE_EVENT", "IN_THE_FINAL_ANALYSIS", "IN_THE_NATURE_OF", "IN_THE_NEIGHBORHOOD_OF", "IN_THE_PROCESS_OF", "IT_SEEMS_OR_APPEARS_THAT", "I_VE_A", "LOOK_FORWARD_NOT_FOLLOWED_BY_TO", "LOOK_SLIKE", "MAJORITY", "MAKE_AN_ATTEMPT", "MAKE_DECISIONS_ABOUT", "NEGATE_MEANING", "NOT_ABLE", "NOT_ACCEPT", "NOT_CERTAIN", "NOT_MANY", "NOT_THE_SAME", "NOT_UNLIKE", "NOT_VERY_OFTEN", "OBTAIN", "ON_THE_OCCASION_OF", "ON_THE_OTHER_HAND", "ON_TWO_SEPARATE_OCCASIONS", "PARAGRAPH_REPEAT_BEGINNING_RULE", "PASSIONATE_BY_ABOUT_2", "PLURAL_THAT_WHICH_WHO_AGREEMENT", "POPULAR_AMONG_WITH", "QUESTION_X_WHETHER", "REFERRING_BACK", "REGARD_AS_BEING", "SERIAL_COMMA_ON", "SINGULAR_NOUN_THAT_AGREEMENT", "SINGULAR_THAT_WHICH_WHO_AGREEMENT", "SOLICIT_FOR", "SOONER_RATHER_THAN_LATER", "STATE-OF-THE-ART", "SUFFICIENT", "SV_AGREEMENT_CLAUSES_PLURAL", "SV_AGREEMENT_CLAUSES_SINGULAR", "TAG_QUESTIONS_2", "TAKE_ACTION_TO", "THAT_EXISTS", "THE_CC", "THE_TRUTH_OR_FACT_IS", "THE_UNDERSIGNED", "THIS_ALL", "TIME_NOW", "TOO_LONG_PARAGRAPH", "TRYNA", "TYPE_OF", "TYPEWRITER_APOSTROPHE", "TYPO_AN_AND", "TYPO_AS_HAS_WAS", "TYPOGRAPHICAL_APOSTROPHE", "TYPO_ID_IF", "TYPO_OR_OF", "TYPO_THEY_S", "UNTIL_SUCH_TIME_AS", "USED_FOR_VBG", "USELESS_THAT", "US_ONE_ENTITY", "VERB_HERE_SINCE", "VERY_SMALL_TINY", "WAITING_MY_PATIENT", "WHAT_I_MEAN_TO_SAY_IS_THAT", "WHITESPACE_PARAGRAPH", "WHITESPACE_PARAGRAPH_BEGIN", "WH_PHRASE_AGREEMENT", "WIKIPEDIA_12_AM", "WIKIPEDIA_12_PM", "WIKIPEDIA_CONTRACTIONS", "WIKIPEDIA_CURRENTLY", "WITH_OR_IN_REFERENCE_OR_REGARD_TO", "WORLD_AROUND_IT", "WORTHWHILE", "YOU_ARE_REQUESTED", },
      }, -- }}}
      disabledRules = {
        ["en-US"] = {
          "ELLIPSIS", -- Prefer plain text
          "EN_QUOTES",  -- Pandoc converts stuff to proper quotes anyway
          "MORFOLOGIK_RULE_EN_US",  -- Use built-in spell checker
          "NON_STANDARD_COMMA",  -- Flags Chinese punctuation
          "NON_STANDARD_QUESTION_MARK",  -- Flags Chinese punctuation
          "PASSIVE_VOICE", -- Too noisy
          "PUNCTUATION_PARAGRAPH_END", -- Issues w/ Markdown lists
          "PARAGRAPH_REPEAT_BEGINNING_RULE", -- Clashes with titles
          "SO_AS_TO",
        },
      },
      additionalRules = {
        enablePickyRules = true,
        motherTongue = "zh-CN",
      },
    },
  },
  get_language_id = function(_, filetype)
    local language_id_mapping = {
      ["vimwiki.markdown.pandoc"] = "markdown",
      bib = "bibtex",
      plaintex = "tex",
      rnoweb = "sweave",
      rst = "restructuredtext",
      tex = "latex",
      xhtml = "xhtml",
    }
    local language_id = language_id_mapping[filetype]
    if language_id then
      return language_id
    else
      return filetype
    end
  end,
}
require("lspconfig").clangd.setup {
  on_attach = on_attach,
}
require("lspconfig").zk.setup {
  on_attach = on_attach,
}
EOF
" }}}2
```

### Miscellaneous ###

I use FZF instead of, say, `telescope.nvim` for fuzzy finding as the `notational-fzf-vim` plugin mentioned above requires FZF.

The following function allows FZF to search in the location list with previews enabled. This is particularly useful when paired with `:VimwikiBacklinks` and `:BacklinksFillPreview` implemented above to get an overview of pages linking to the current document.

Implementation-wise, it is somewhat of a hack -- it writes the location list to a temporary file in `vimgrep` format and invokes `cat $TEMP_FILE`, treating it as a `grep` command.

``` vim
" FZF {{{2
function! s:fzfloc()
  let loclist = getloclist(0)
  lclose
  let lines = map(loclist,
    \ 'printf("%s:%s:%s:%s",
    \  bufname(v:val["bufnr"]), v:val["lnum"], v:val["col"], v:val["text"])')
  let tmpfile = tempname()
  call writefile(lines, tmpfile)
  call fzf#vim#grep('cat ' . tmpfile, 1, fzf#vim#with_preview(), 0)
endfunction
command! Loc call s:fzfloc()
" }}}2
```

I find the myriad of `g*` and `z*` bindings difficult to remember. Having `which-key.nvim` to serve as a hint is thus fairly useful. Another awesome feature of this plugin is that it shows the register contents or mark targets when `<C-R>`/`'` is pressed.

``` vim
" which-key.nvim {{{2
lua << EOF
require("which-key").setup { }
EOF
" }}}2
```

``` vim
" }}}1
```
