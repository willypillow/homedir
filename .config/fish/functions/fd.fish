function fd
  if which fd >/dev/null;
    command fd $argv
  else
    fdfind $argv
  end
end
