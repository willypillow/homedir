function rgg --wraps=rg\ -i\ --pre-glob\ \'\*.\{pdf,xl\[ts\]\[bxm\],xl\[st\],do\[ct\],do\[ct\]\[xm\],p\[po\]t\[xm\],p\[op\]t,html,epub\}\'\ --pre\ rgpre --description alias\ rgg=rg\ -i\ --pre-glob\ \'\*.\{pdf,xl\[ts\]\[bxm\],xl\[st\],do\[ct\],do\[ct\]\[xm\],p\[po\]t\[xm\],p\[op\]t,html,epub\}\'\ --pre\ rgpre
  rg -i --pre-glob '*.{pdf,xl[ts][bxm],xl[st],do[ct],do[ct][xm],p[po]t[xm],p[op]t,html,epub}' --pre rgpre $argv; 
end
