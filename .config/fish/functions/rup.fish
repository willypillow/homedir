function rup --wraps='rclone sync -vvv ~/sync/vimwiki vimwiki:' --description 'alias rup=rclone sync -vvv ~/sync/vimwiki vimwiki:'
  rclone sync -vvv ~/sync/vimwiki vimwiki: $argv; 
end
