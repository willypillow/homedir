function rdown --wraps='rclone sync -vvv vimwiki: ~/sync/vimwiki' --description 'alias rdown=rclone sync -vvv vimwiki: ~/sync/vimwiki'
  rclone sync -vvv vimwiki: ~/sync/vimwiki $argv; 
end
