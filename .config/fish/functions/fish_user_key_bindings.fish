function fish_user_key_bindings
	function __bind
		bind "$argv[1]" "$argv[2]"
		bind -M insert "$argv[1]" "$argv[2]"
		bind -M visual "$argv[1]" "$argv[2]"
	end

	__bind \e\[1\;5C forward-word
	__bind \e\[1\;5D backward-word
	__bind \ef forward-word
	__bind \eb backward-word
	__bind \ch backward-kill-word
	__bind \e\x7f backward-kill-word

	fzf_key_bindings
end
