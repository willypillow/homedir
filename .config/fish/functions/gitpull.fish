function gitpull --wraps='git pull --depth=1; git tag -d (git tag -l); git reflog expire --expire=all --all; git gc --prune=now --aggressive' --description 'alias gitpull=git pull --depth=1; git tag -d (git tag -l); git reflog expire --expire=all --all; git gc --prune=now --aggressive'
  git pull --depth=1; git tag -d (git tag -l); git reflog expire --expire=all --all; git gc --prune=now --aggressive $argv; 
end
