function git_fetch_all --wraps='git fetch (git remote) "*:*"' --description 'alias git_fetch_all=git fetch (git remote) "*:*"'
  git fetch (git remote) "*:*" $argv
        
end
