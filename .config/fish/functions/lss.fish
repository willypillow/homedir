function lss --wraps='sudo ls /.snapshots/ /home/.snapshots/' --description 'alias lss=sudo ls /.snapshots/ /home/.snapshots/'
  sudo ls /.snapshots/ /home/.snapshots/ $argv; 
end
