function net --wraps='sudo netctl stop-all; sudo netctl start (ls /etc/netctl | fzf)' --description 'alias net=sudo netctl stop-all; sudo netctl start (ls /etc/netctl | fzf)'
  sudo netctl stop-all; sudo netctl start (ls /etc/netctl | fzf) $argv; 
end
