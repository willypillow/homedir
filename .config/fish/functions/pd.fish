# Defined in - @ line 1
function pd
	#pandoc -s -o out.pdf -f markdown --listings --template="$HOME/sync/ieee-pandoc-template/template.latex" --pdf-engine=xelatex --csl ~/sync/ieee-pandoc-template/bibliography.csl --citeproc ~/sync/ieee-pandoc-template/metadata.yaml $argv;
	pandoc -s --pdf-engine xelatex --lua-filter=$HOME/.bin/diagram-generator.lua -F pandoc-include -F pandoc-crossref --citeproc ~/sync/dotfiles/pandoc-metadata.yml $argv;
end
