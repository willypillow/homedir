function bdf --wraps='sudo btrfs filesystem usage /' --description 'alias bdf=sudo btrfs filesystem usage /'
  sudo btrfs filesystem usage / $argv; 
end
