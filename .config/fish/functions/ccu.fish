function ccu --wraps='conf add -u; conf commit -m Update.; conf push' --description 'alias ccu=conf add -u; conf commit -m Update.; conf push'
  conf add -u; conf commit -m Update.; conf push $argv; 
end
