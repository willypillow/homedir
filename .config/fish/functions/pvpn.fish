function pvpn
  pass show protonvpn | sudo openvpn --route-up '/usr/bin/sysctl net.ipv6.conf.all.disable_ipv6=1' --route-pre-down '/usr/bin/sysctl net.ipv6.conf.all.disable_ipv6=0' --config (sudo find /etc/openvpn/client/ -type f | fzf) --auth-user-pass /dev/stdin $argv; 
end
