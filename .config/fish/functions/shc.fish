function shc --wraps='sudo snapper -c home delete -s (sudo snapper -c home --machine-readable csv list --columns number | tail +3)' --description 'alias shc=sudo snapper -c home delete -s (sudo snapper -c home --machine-readable csv list --columns number | tail +3)'
  sudo snapper -c home delete -s (sudo snapper -c home --machine-readable csv list --columns number | tail +3) $argv; 
end
