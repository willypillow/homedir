function sus --wraps='sudo systemctl suspend' --description 'alias sus=sudo systemctl suspend'
  sudo systemctl suspend $argv; 
end
