function gcu --wraps='git add -u; git commit -m Update.; git push' --description 'alias gcu=git add -u; git commit -m Update.; git push'
  git add -u; git commit -m Update.; git push $argv; 
end
