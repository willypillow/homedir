function src --wraps='sudo snapper -c root delete -s (sudo snapper -c root --machine-readable csv list --columns number | tail +3)' --description 'alias src=sudo snapper -c root delete -s (sudo snapper -c root --machine-readable csv list --columns number | tail +3)'
  sudo snapper -c root delete -s (sudo snapper -c root --machine-readable csv list --columns number | tail +3) $argv; 
end
