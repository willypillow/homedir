function bat
  if which bat >/dev/null;
    command bat $argv
  else
    batcat $argv
  end
end
