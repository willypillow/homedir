if status --is-interactive;
	set -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
	set -x GPG_TTY (tty)
	gpg-connect-agent updatestartuptty /bye > /dev/null

	zoxide init fish | source

	eval (starship init fish)
end
