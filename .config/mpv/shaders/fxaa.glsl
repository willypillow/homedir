//Anime4K GLSL v1.0 Release Candidate 2

// MIT License

// Copyright (c) 2019 bloc97, DextroseRe

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

//!DESC Anime4K-Luma-v1.0RC2
//!HOOK LUMA
//!BIND HOOKED
//!WIDTH OUTPUT.w
//!HEIGHT OUTPUT.h
//!SAVE LUMAX
//!COMPONENTS 1

vec4 hook() {
	return HOOKED_tex(HOOKED_pos);
}

//!DESC Anime4K-ComputeGaussianX-v1.0RC2
//!HOOK LUMA
//!BIND HOOKED
//!BIND LUMAX
//!SAVE LUMAG
//!COMPONENTS 1

float lumGaussian7(vec2 pos, vec2 d) {
	float g = LUMA_tex(pos - (d * 3)).x * 0.121597;
	g = g + LUMA_tex(pos - (d * 2)).x * 0.142046;
	g = g + LUMA_tex(pos - d).x * 0.155931;
	g = g + LUMA_tex(pos).x * 0.160854;
	g = g + LUMA_tex(pos + d).x * 0.155931;
	g = g + LUMA_tex(pos + (d * 2)).x * 0.142046;
	g = g + LUMA_tex(pos + (d * 3)).x * 0.121597;
	
	return clamp(g, 0, 1); //Clamp for sanity check
}

vec4 hook() {
	float g = lumGaussian7(HOOKED_pos, vec2(LUMAX_pt.x, 0));
    return vec4(g, 0, 0, 0);
}



//!DESC Anime4K-ComputeGaussianY-v1.0RC2
//!HOOK LUMA
//!BIND HOOKED
//!BIND LUMAX
//!BIND LUMAG
//!SAVE LUMAG
//!COMPONENTS 1

float lumGaussian7(vec2 pos, vec2 d) {
	float g = LUMAG_tex(pos - (d * 3)).x * 0.121597;
	g = g + LUMAG_tex(pos - (d * 2)).x * 0.142046;
	g = g + LUMAG_tex(pos - d).x * 0.155931;
	g = g + LUMAG_tex(pos).x * 0.160854;
	g = g + LUMAG_tex(pos + d).x * 0.155931;
	g = g + LUMAG_tex(pos + (d * 2)).x * 0.142046;
	g = g + LUMAG_tex(pos + (d * 3)).x * 0.121597;
	
	return clamp(g, 0, 1); //Clamp for sanity check
}

vec4 hook() {
	float g = lumGaussian7(HOOKED_pos, vec2(0, LUMAX_pt.y));
    return vec4(g, 0, 0, 0);
}


//!DESC Anime4K-LineDetect-v1.0RC2
//!HOOK LUMA
//!BIND HOOKED
//!BIND LUMAG
//!SAVE LUMAG
//!COMPONENTS 1

#define BlendColorDodgef(base, blend) 	(((blend) == 1.0) ? (blend) : min((base) / (1.0 - (blend)), 1.0))
#define BlendColorDividef(top, bottom) 	(((bottom) == 1.0) ? (bottom) : min((top) / (bottom), 1.0))

// Component wise blending
#define Blend(base, blend, funcf) 		vec3(funcf(base.r, blend.r), funcf(base.g, blend.g), funcf(base.b, blend.b))
#define BlendColorDodge(base, blend) 	Blend(base, blend, BlendColorDodgef)


vec4 hook() {
	float lum = clamp(LUMA_tex(HOOKED_pos).x, 0.001, 0.999);
	float lumg = clamp(LUMAG_tex(HOOKED_pos).x, 0.001, 0.999);
	
	float pseudolines = BlendColorDividef(lum, lumg);
	pseudolines = 1 - clamp(pseudolines - 0.05, 0, 1);
	
    return vec4(pseudolines, 0, 0, 0);
}



//!DESC Anime4K-ComputeLineGaussianX-v1.0RC2
//!HOOK LUMA
//!BIND HOOKED
//!BIND LUMAX
//!BIND LUMAG
//!SAVE LUMAG
//!COMPONENTS 1

float lumGaussian7(vec2 pos, vec2 d) {
	float g = LUMAG_tex(pos - (d * 3)).x * 0.121597;
	g = g + LUMAG_tex(pos - (d * 2)).x * 0.142046;
	g = g + LUMAG_tex(pos - d).x * 0.155931;
	g = g + LUMAG_tex(pos).x * 0.160854;
	g = g + LUMAG_tex(pos + d).x * 0.155931;
	g = g + LUMAG_tex(pos + (d * 2)).x * 0.142046;
	g = g + LUMAG_tex(pos + (d * 3)).x * 0.121597;
	
	return clamp(g, 0, 1); //Clamp for sanity check
}

vec4 hook() {
	float g = lumGaussian7(HOOKED_pos, vec2(LUMAX_pt.x, 0));
    return vec4(g, 0, 0, 0);
}



//!DESC Anime4K-ComputeLineGaussianY-v1.0RC2
//!HOOK LUMA
//!BIND HOOKED
//!BIND LUMAX
//!BIND LUMAG
//!SAVE LUMAG
//!COMPONENTS 1

float lumGaussian7(vec2 pos, vec2 d) {
	float g = LUMAG_tex(pos - (d * 3)).x * 0.121597;
	g = g + LUMAG_tex(pos - (d * 2)).x * 0.142046;
	g = g + LUMAG_tex(pos - d).x * 0.155931;
	g = g + LUMAG_tex(pos).x * 0.160854;
	g = g + LUMAG_tex(pos + d).x * 0.155931;
	g = g + LUMAG_tex(pos + (d * 2)).x * 0.142046;
	g = g + LUMAG_tex(pos + (d * 3)).x * 0.121597;
	
	return clamp(g, 0, 1); //Clamp for sanity check
}

vec4 hook() {
	float g = lumGaussian7(HOOKED_pos, vec2(0, LUMAX_pt.y));
    return vec4(g, 0, 0, 0);
}



//Fast FXAA (1 Iteration) courtesy of Geeks3D
//https://www.geeks3d.com/20110405/fxaa-fast-approximate-anti-aliasing-demo-glsl-opengl-test-radeon-geforce/3/

//!DESC Anime4K-PostFXAA-v1.0RC2
//!HOOK SCALED
//!BIND HOOKED
//!BIND LUMA
//!BIND LUMAG

#define FXAA_MIN (1.0 / 128.0)
#define FXAA_MUL (1.0 / 8.0)
#define FXAA_SPAN 8.0

#define LINE_DETECT_MUL 6
#define LINE_DETECT_THRESHOLD 0.06

#define strength (min((SCALED_size.x) / (LUMA_size.x), 1))
#define lineprob (LUMAG_tex(HOOKED_pos).x)

vec4 getAverage(vec4 cc, vec4 xc) {
	float prob = clamp(lineprob, 0, 1);
	if (prob < LINE_DETECT_THRESHOLD) {
		prob = 0;
	}
	float realstrength = clamp(strength * prob * LINE_DETECT_MUL, 0, 1);
	return cc * (1 - realstrength) + xc * realstrength;
}

float getLum(vec4 rgb) {
	return (rgb.r + rgb.r + rgb.g + rgb.g + rgb.g + rgb.b) / 6;
}

vec4 hook()  {

	if (lineprob < LINE_DETECT_THRESHOLD) {
		return HOOKED_tex(HOOKED_pos);
	}


	vec2 d = HOOKED_pt;
	
	
    vec4 cc = HOOKED_tex(HOOKED_pos);
    vec4 xc = cc;
	
	float t = HOOKED_tex(HOOKED_pos + vec2(0, -d.y)).x;
	float l = HOOKED_tex(HOOKED_pos + vec2(-d.x, 0)).x;
	float r = HOOKED_tex(HOOKED_pos + vec2(d.x, 0)).x;
	float b = HOOKED_tex(HOOKED_pos + vec2(0, d.y)).x;
	
    float tl = HOOKED_tex(HOOKED_pos + vec2(-d.x, -d.y)).x;
    float tr = HOOKED_tex(HOOKED_pos + vec2(d.x, -d.y)).x;
    float bl = HOOKED_tex(HOOKED_pos + vec2(-d.x, d.y)).x;
    float br = HOOKED_tex(HOOKED_pos + vec2(d.x, d.y)).x;
    float cl  = HOOKED_tex(HOOKED_pos).x;
	
    float minl = min(cl, min(min(tl, tr), min(bl, br)));
    float maxl = max(cl, max(max(tl, tr), max(bl, br)));
    
    vec2 dir = vec2(- tl - tr + bl + br, tl - tr + bl - br);
    
    float dirReduce = max((tl + tr + bl + br) *
                          (0.25 * FXAA_MUL), FXAA_MIN);
    
    float rcpDirMin = 1.0 / (min(abs(dir.x), abs(dir.y)) + dirReduce);
    dir = min(vec2(FXAA_SPAN, FXAA_SPAN),
              max(vec2(-FXAA_SPAN, -FXAA_SPAN),
              dir * rcpDirMin)) * d;
    
    vec4 rgbA = 0.5 * (
        HOOKED_tex(HOOKED_pos + dir * -(1.0/6.0)) +
        HOOKED_tex(HOOKED_pos + dir * (1.0/6.0)));
    vec4 rgbB = rgbA * 0.5 + 0.25 * (
        HOOKED_tex(HOOKED_pos + dir * -0.5) +
        HOOKED_tex(HOOKED_pos + dir * 0.5));

		
    float lumb = getLum(rgbB);
	
    if ((lumb < minl) || (lumb > maxl)) {
        xc = rgbA;
    } else {
        xc = rgbB;
	}
    return getAverage(cc, xc);
}
