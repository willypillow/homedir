-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")
local dpi = require("beautiful.xresources").apply_dpi
local lain = require("lain")
local machi = require("layout-machi")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init("~/.config/awesome/theme.lua")

beautiful.layout_machi = machi.get_icon()

-- This is used later as the default terminal and editor to run.
terminal = "uxterm fish"
editor = os.getenv("EDITOR") or "nano"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

lain.layout.termfair.center.nmaster = 3
lain.layout.termfair.center.ncol = 2

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.fair,
    machi.default_layout,
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier,
    awful.layout.suit.corner.nw,
    awful.layout.suit.corner.ne,
    awful.layout.suit.corner.sw,
    awful.layout.suit.corner.se,
    lain.layout.termfair,
    lain.layout.termfair.center,
    lain.layout.cascade,
    lain.layout.cascade.tile,
    lain.layout.centerwork.horizontal,
    lain.layout.centerwork,
}
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end },
}

layoutmenu = {}
for k, layout in pairs(awful.layout.layouts) do
    layoutmenu[k] = { layout.name, function() awful.layout.set(layout) end }
end

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "layouts", layoutmenu },
                                    { "open terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
menubar.menu_gen.all_menu_dirs = {
    "/usr/share/applications/",
    "/usr/local/share/applications",
    os.getenv("HOME") .. "/.local/share/applications",
    os.getenv("HOME") .. "/.local/share/flatpak/exports/share/applications",
    "/var/lib/flatpak/exports/share/applications"
}
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Helper functions
-- Screen recorder
local screen_record_pid = 0

local function screen_record_start(filename)
    naughty.notify({ text = "Recording started" })
    screen_record_pid = awful.spawn.with_shell(
        'ffmpeg -f x11grab -s 1920x1080 -i :0 -framerate 30 -c:v libvpx -b:v 10M -pix_fmt yuv420p -crf 8 -deadline realtime ~/Downloads/$(date +%s).webm'
    )
end

local function screen_record_stop()
    naughty.notify({ text = "Recording stopped" })
    awful.spawn("kill -15 " .. screen_record_pid)
    screen_record_pid = 0
end

-- Center popup for layouts and tags
local center_popup = awful.popup {
    widget = wibox.container.margin(wibox.widget.textbox()),
    ontop = true,
    visible = false,
    placement = awful.placement.centered,
    shape = gears.shape.rounded_rect
}
center_popup.widget.margins = dpi(8)
center_popup.widget.widget.font = "Noto Sans 32"

local popup_time = timer({ timeout = 1 })
popup_time:connect_signal("timeout",
    function()
        center_popup.visible = false
        popup_time:stop()
    end)

local function center_popup_display(text)
    center_popup.widget.widget:set_text(text)
    center_popup.visible = true
    center_popup.screen = awful.screen.focused()
    popup_time:again()
end

-- }}}

-- {{{ Wibar
-- Brightness widget
local max_bright_file = io.open("/sys/class/backlight/amdgpu_bl1/max_brightness", "r")
local max_bright = tonumber(max_bright_file:read("*a"))
max_bright_file:close()
local brightness = wibox.widget.textbox()
local function adj_brightness(delta)
    return function (c)
        local bright_file = io.open("/sys/class/backlight/amdgpu_bl1/brightness", "w+")
        local num = tonumber(bright_file:read("*a")) + delta
        bright_file:write(num)
        bright_file:close()
        local bright_file = io.open("/sys/class/backlight/amdgpu_bl1/brightness", "r")
        num = tonumber(bright_file:read("*a"))
        bright_file:close()
        brightness:set_text("|B" .. math.floor(num * 100 / max_bright))
    end
end
adj_brightness(0)(nil)

-- Volume widget
local volume = wibox.widget.textbox()
local function update_vol()
    awful.spawn.easy_async("amixer get Master", function(stdout, stderr, reason, exit_code)
        local stat = stdout:match("off")
        if stat ~= nil then
            volume:set_text("|V--")
        else
            local str = stdout:match("%[[^%]]*%%%]"):sub(2, -3)
            volume:set_text("|V" .. str)
        end
    end)
end
update_vol()

-- Battery widget and other stuff needing periodic updates
local battery = wibox.widget.textbox()
local function update_widgets()
    local capf = io.open("/sys/class/power_supply/BAT0/capacity", "r")
    local statf = io.open("/sys/class/power_supply/BAT0/status", "r")
    if capf and statf then
        local cap, stat = capf:read("*a"), statf:read("*a")
        local perc = cap:match("[^\n]*")
        if stat ~= "Discharging\n" then
            battery:set_text("|P" .. perc .. "+")
        else
            battery:set_text("|P" .. perc)
        end
    end
    capf:close()
    statf:close()
end
update_widgets()
local widgets_time = timer({ timeout = 10 })
widgets_time:connect_signal("timeout", update_widgets)
widgets_time:start()

-- Create a textclock widget
mytextclock = wibox.widget.textclock("|%m-%d %H:%M %a|")

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = beautiful.menu_width } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, false)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    local default_layout = 1
    if s.geometry.height < s.geometry.width then
        default_layout = 2
    end

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[default_layout])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.noempty,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "right", screen = s, width = beautiful.menu_height })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.container.rotate,
        direction = "west",
        {
            layout = wibox.layout.align.horizontal,
            { -- Left widgets
                layout = wibox.layout.fixed.horizontal,
                mylauncher,
                s.mytaglist,
                s.mypromptbox,
            },
            s.mytasklist, -- Middle widget
            { -- Right widgets
                layout = wibox.layout.fixed.horizontal,
                brightness,
                battery,
                volume,
                mytextclock,
                wibox.widget.systray(),
                wibox.container.rotate(s.mylayoutbox, "east"),
            },
        }
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.bydirection("down")
            if client.focus then client.focus:raise() end
        end,
        {description = "focus down", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.bydirection("up")
            if client.focus then client.focus:raise() end
        end,
        {description = "focus up", group = "client"}
    ),
    awful.key({ modkey,           }, "h",
        function ()
            awful.client.focus.bydirection("left")
            if client.focus then client.focus:raise() end
        end,
        {description = "focus left", group = "client"}
    ),
    awful.key({ modkey,           }, "l",
        function ()
            awful.client.focus.bydirection("right")
            if client.focus then client.focus:raise() end
        end,
        {description = "focus right", group = "client"}
    ),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
              {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.bydirection("down")    end,
              {description = "swap with client below", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.bydirection("up")    end,
              {description = "swap with client above", group = "client"}),
    awful.key({ modkey, "Shift"   }, "h", function () awful.client.swap.bydirection("left")    end,
              {description = "swap with client on the left", group = "client"}),
    awful.key({ modkey, "Shift"   }, "l", function () awful.client.swap.bydirection("right")    end,
              {description = "swap with client on the right", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_bydirection("down") end,
              {description = "focus the screen below", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_bydirection("up") end,
              {description = "focus the screen above", group = "screen"}),
    awful.key({ modkey, "Control" }, "h", function () awful.screen.focus_bydirection("left") end,
              {description = "focus the screen on the left", group = "screen"}),
    awful.key({ modkey, "Control" }, "l", function () awful.screen.focus_bydirection("right") end,
              {description = "focus the screen on the right", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "=",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "-",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "=",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "-",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "=",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "-",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"}),

    awful.key({ modkey,           }, ".",     function () machi.default_editor.start_interactive() end,
              {description = "edit the current layout if it is a machi layout", group = "layout"}),
    awful.key({ modkey,           }, "/",     function () machi.switcher.start(client.focus) end,
              {description = "switch between windows for a machi layout", group = "layout"}),

    -- Brightness, volume, screenshot, ...etc
    awful.key({ modkey }, "F3", adj_brightness(-5)),
    awful.key({ }, "XF86MonBrightnessDown", adj_brightness(-5)),
    awful.key({ modkey }, "F4", adj_brightness(5)),
    awful.key({ }, "XF86MonBrightnessUp", adj_brightness(5)),
    awful.key({ }, "XF86AudioRaiseVolume",
        function ()
            awful.spawn("amixer set Master 2%+")
            update_vol()
        end),
    awful.key({ }, "XF86AudioLowerVolume",
        function ()
            awful.spawn("amixer set Master 2%-")
            update_vol()
        end),
    awful.key({ }, "XF86AudioMute",
        function ()
            awful.spawn("amixer sset Master toggle")
            update_vol()
        end),
    awful.key({ modkey }, "F11",
        function ()
            awful.spawn("amixer set Master 2%+")
            update_vol()
        end),
    awful.key({ modkey }, "F10",
        function ()
            awful.spawn("amixer set Master 2%-")
            update_vol()
        end),
    awful.key({ modkey }, "F12",
        function ()
            awful.spawn("amixer sset Master toggle")
            update_vol()
        end),
    awful.key({ }, "Print",
        function ()
            awful.spawn.with_shell('maim ~/Downloads/$(date +%s).png')
        end,
        {description = "screenshot", group = "launcher"}
    ),
    awful.key({ "Shift" }, "Print",
        function ()
            awful.spawn.easy_async_with_shell(
                'maim -s | curl -F "file=@-;filename=screenshot.png" -F "key=<"<(pass show upload.systems | tr -d "\n") https://api.upload.systems/images/upload | jq -r .url',
                function (url)
                    awful.spawn.with_shell("echo '" .. url .. "' | xclip -sel clip")
                    naughty.notify({ text = "URL: " .. url })
                end)
        end,
        {description = "screenshot region and upload", group = "launcher"}
    ),
    awful.key({ "Control" }, "Print",
        function ()
            awful.spawn.with_shell('maim -s ~/Downloads/$(date +%s).png')
        end,
        {description = "screenshot region", group = "launcher"}
    ),
    awful.key({ modkey }, "Print",
        function ()
            if screen_record_pid == 0 then
                screen_record_start()
            else
                screen_record_stop()
            end
        end,
        {description = "screen record", group = "launcher"}
    ),
    awful.key({ modkey, "Shift" }, "r", function () lain.util.rename_tag() end,
              {description = "rename tag", group = "tag"}),
    awful.key({ modkey }, "F1",
        function()
            awful.spawn("xrandr --auto")
        end,
        {description = "auto display", group = "screen"}
    ),
    awful.key({ modkey }, "F2",
        function()
            -- One of the following command should work
            -- Depending on if we're on Nvidia or nouveau
            awful.spawn("xrandr --output DP-0 --auto --rotate left --mode 2560x1440_72.00_rb2 --output HDMI-0 --auto --primary --left-of DP-0 --mode 1920x1080_72.00_rb2 --output eDP-1-1 --auto --left-of HDMI-0 --mode 1920x1080_144.00_rb2")
            awful.spawn("xrandr --output DP-1-2 --auto --rotate left --output HDMI-1-1 --auto --primary --left-of DP-1-2 --output eDP-1 --auto --left-of HDMI-1-1 --mode 1920x1080")
        end,
        {description = "external display", group = "screen"}
    ),
    awful.key({ modkey }, "v",
        function()
            awful.spawn.with_shell('mpv "$(xclip -o)"')
            naughty.notify({ text = "mpv launched" })
        end,
        {description = "play with mpv", group = "launcher"}
    ),
    awful.key({ modkey }, "'",
        function()
            awful.spawn(terminal .. " -e nvim")
        end,
        {description = "start text editor", group = "launcher"}
    ),
    awful.key({ modkey, "Shift"   }, "f",
        function()
            lain.util.menu_clients_current_tags()
        end,
        {description = "clients menu (current tags)", group = "launcher"}
    ),
    awful.key({ modkey, "Control" }, "f",
        function()
            awful.menu.client_list()
        end,
        {description = "clients menu", group = "launcher"}
    ),
    awful.key({ modkey }, ";",
        function()
            awful.spawn("i3lock -c 3b240a")
        end,
        {description = "lock screen", group = "launcher"}
    )
)

clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),
    awful.key({ modkey, "Control" }, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"}),

    awful.key({ modkey, "Control" }, "t",
        function (c)
            if not c.titlebar then
                c:emit_signal("request::titlebars")
            else
                awful.titlebar.toggle(c)
            end
            c.titlebar = not c.titlebar
        end,
        {description = "toggle titlebar", group = "client"}
    ),
    awful.key({ modkey }, "b",
        function ()
            mouse.screen.mywibox.visible = not mouse.screen.mywibox.visible
        end,
        {description = "toggle wibox", group = "layout"}
    ),
    awful.key({ modkey, "Control", "Shift" }, "m", lain.util.magnify_client,
              {description = "(un)magnify client", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  i == 1 and {description = "view tag #x", group = "tag"} or nil),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  i == 1 and {description = "toggle tag #x", group = "tag"} or nil),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  i == 1 and {description = "move focused client to tag #x", group = "tag"} or nil),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  i == 1 and {description = "toggle focused client on tag #x", group = "tag"} or nil)
    )
end

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end),

    awful.button({ modkey }, 2, function (c) c:kill() end),
    awful.button({ modkey }, 4, function(c) c.opacity = c.opacity - 0.05 end),
    awful.button({ modkey }, 5, function(c) c.opacity = c.opacity + 0.05 end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen,
                     maximized_vertical = false,
                     maximized_horizontal = false,
                     size_hints_honor = false,
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer"},

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to dialogs
    { rule_any = { type = { "dialog" }
      }, properties = { titlebars_enabled = true }
    },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- Hide borders for maximized windows
screen.connect_signal("arrange", function(s)
    local layout = s.selected_tag.layout.name
    local tiled_clients = #s.tiled_clients
    for _, c in pairs(s.clients) do
        if (layout == "floating" or (tiled_clients > 1 and layout ~= "max") or c.floating) and not c.maximized then
            c.border_width = beautiful.border_width
        else
            c.border_width = 0
        end
    end
end)

screen.connect_signal("tag::history::update", function(s)
    local tags = {}
    for k, t in pairs(s.selected_tags) do
        tags[k] = t.name
    end
    center_popup_display(table.concat(tags, ', '))
end)

tag.connect_signal("property::layout", function(t)
    center_popup_display(awful.layout.get(t.screen).name)
end)
-- }}}
