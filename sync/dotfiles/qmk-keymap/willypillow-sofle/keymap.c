#ifndef __clang__
#    include QMK_KEYBOARD_H
#else
#    include "sofle.h"
#endif

enum Layer {
    _QWERTY = 0,
    _DVORAK,
    _LOWER,
    _RAISE,
    _SYM,
    _ADJUST,
};

#define CTL_ESC CTL_T(KC_ESC)
#define CTL_BSH RCTL_T(KC_BSLS)
#define OSM_SFT OSM(MOD_LSFT)
#define RSFT_BS RSFT_T(KC_BSPC)

#define LO_ENT LT(_LOWER, KC_ENT)
#define RA_SPC LT(_RAISE, KC_SPACE)
#define ADJ_EQL LT(_ADJUST, KC_EQL)
#define TG_LOW TG(_LOWER)
#define TG_RAI TG(_RAISE)

#define PG_BCK LALT(KC_LEFT)
#define PG_FWD LALT(KC_RIGHT)
#define PRV_WD C(KC_LEFT)
#define NXT_WD C(KC_RGHT)
#define DEL_WD C(KC_BSPC)
#define COPY C(KC_INS)
#define PASTE S(KC_INS)
#define CUT S(KC_DEL)

#define ALT_A LALT_T(KC_A)
#define GUI_S LGUI_T(KC_S)
#define SFT_D LSFT_T(KC_D)
#define CTL_F LCTL_T(KC_F)
#define CTL_J RCTL_T(KC_J)
#define SFT_K RSFT_T(KC_K)
#define GUI_L RGUI_T(KC_L)
#define ALT_SCL RALT_T(KC_SCLN)

#define GUI_O LGUI_T(KC_O)
#define SFT_E LSFT_T(KC_E)
#define CTL_U LCTL_T(KC_U)
#define CTL_H RCTL_T(KC_H)
#define SHFT_T RSFT_T(KC_T)
#define GUI_N RGUI_T(KC_N)
#define ALT_S RALT_T(KC_S)

#ifdef MAGIC_KEYCODE_ENABLE
#    define NKRO MAGIC_TOGGLE_NKRO
#else
#    define NKRO _______
#endif

enum Keycode {
    DVORAK = SAFE_RANGE,
};

// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/*      _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, */
/*                        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______                    */
    [_QWERTY] = LAYOUT( \
        KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                      KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    ADJ_EQL, \
        KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,                      KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_MINS, \
        DEL_WD,  ALT_A,   GUI_S,   SFT_D,   CTL_F,   KC_G,                      KC_H,    CTL_J,   SFT_K,   GUI_L,   ALT_SCL, KC_QUOT, \
        KC_LBRC, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_MUTE, DM_PLY1, KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RBRC, \
                          CTL_ESC, KC_LALT, KC_LGUI, OSM_SFT, LO_ENT,  RA_SPC,  RSFT_BS, KC_RGUI, KC_RALT, CTL_BSH                    \
    ),
    [_DVORAK] = LAYOUT( \
        KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                      KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    ADJ_EQL, \
        KC_TAB,  KC_QUOT, KC_COMM, KC_DOT,  KC_P,    KC_Y,                      KC_F,    KC_G,    KC_C,    KC_R,    KC_L,    KC_SLSH, \
        DEL_WD,  ALT_A,   GUI_O,   SFT_E,   CTL_U,   KC_I,                      KC_D,    CTL_H,   SHFT_T,  GUI_N,   ALT_S,   KC_MINS, \
        KC_LBRC, KC_SCLN, KC_Q,    KC_J,    KC_K,    KC_X,    KC_MUTE, DM_PLY1, KC_B,    KC_M,    KC_W,    KC_V,    KC_Z,    KC_RBRC, \
                          CTL_ESC, KC_LALT, KC_LGUI, OSM_SFT, LO_ENT,  RA_SPC,  RSFT_BS, KC_RGUI, KC_RALT, CTL_BSH                    \
    ),
    [_LOWER] = LAYOUT( \
        KC_F12,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,                     KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  \
        KC_CAPS, _______, _______, KC_UP,   KC_TILD, COPY,                      KC_APP,  KC_PSCR, KC_INS,  _______, _______, _______, \
        KC_DEL,  _______, KC_LEFT, KC_DOWN, KC_RGHT, PASTE,                     _______, KC_HOME, KC_PGUP, _______, _______, _______, \
        _______, _______, _______, PRV_WD,  NXT_WD,  CUT,     KC_MPLY, DM_PLY2, _______, KC_END,  KC_PGDN, _______, _______, _______, \
                          _______, _______, _______, _______, _______, _______, _______, _______, _______, _______                    \
    ),
    [_RAISE] = LAYOUT( \
        _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
        KC_TILD, KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC,                   KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_PAST, \
        _______, _______, KC_WH_L, KC_MS_U, KC_WH_R, KC_WH_U,                   _______, KC_P7,   KC_P8,   KC_P9,   KC_P0,   KC_PSLS, \
        _______, _______, KC_MS_L, KC_MS_D, KC_MS_R, KC_WH_D, KC_MPLY, DM_PLY2, _______, KC_P4,   KC_P5,   KC_P6,   KC_PPLS, KC_PMNS, \
                          _______, KC_BTN2, KC_BTN3, KC_BTN1, _______, _______, KC_ACL0, KC_P1,   KC_P2,   KC_P3                      \
    ),
    [_SYM] = LAYOUT( \
        _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
        KC_TILD, KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC,                   KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_PLUS, \
        KC_GRV,  KC_4,    KC_2,    KC_3,    KC_1,    KC_5,                      KC_6,    KC_0,    KC_8,    KC_9,    KC_7,    KC_EQL,  \
        _______, KC_MINS, KC_UNDS, KC_BSLS, KC_LBRC, KC_LCBR, KC_MPLY, DM_PLY2, KC_RCBR, KC_RBRC, KC_PIPE, KC_QUES, KC_SLSH, _______, \
                          _______, _______, _______, _______, _______, _______, _______, _______, _______, _______                    \
    ),
    [_ADJUST] = LAYOUT( \
        RESET,   KC_ASDN, KC_ASUP, KC_ASRP, KC_ASTG, _______,                   _______, _______, _______, _______, _______, _______, \
        DVORAK,  _______, _______, _______, _______, _______,                   _______, _______, _______, _______, KC_NLCK, DM_REC1, \
        NKRO,    _______, _______, _______, _______, _______,                   _______, _______, _______, _______, KC_SLCK, DM_REC2, \
        _______, _______, _______, PG_BCK,  PG_FWD,  _______, _______, _______, _______, _______, _______, _______, KC_PAUS, DM_RSTP, \
                          _______, _______, _______, _______, _______, _______, _______, _______, _______, _______                    \
    ),
};
// clang-format on

#ifdef OLED_DRIVER_ENABLE

static void render_logo(void) {
    static const char PROGMEM qmk_logo[] = {0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0};

    oled_write_P(qmk_logo, false);
}

#    ifdef SWAP_HANDS_ENABLE
extern bool swap_hands;
#    endif

static void print_status_narrow(void) {
    // Print current mode
    oled_write_ln_P(PSTR("MODE"), false);
    switch (get_highest_layer(default_layer_state)) {
        case _QWERTY:
            oled_write_ln_P(PSTR("Qwrt"), false);
            break;
        case _DVORAK:
            oled_write_ln_P(PSTR("Dvor"), false);
            break;
        default:
            oled_write_P(PSTR("----"), false);
    }
    oled_write_P(PSTR("\n\n"), false);
    // Print current layer
    oled_write_P(PSTR("LAYER"), false);
    switch (get_highest_layer(layer_state)) {
        case _QWERTY:
        case _DVORAK:
            oled_write_P(PSTR("Base\n"), false);
            break;
        case _RAISE:
            oled_write_P(PSTR("Raise"), false);
            break;
        case _LOWER:
            oled_write_P(PSTR("Lower"), false);
            break;
        case _SYM:
            oled_write_P(PSTR("Sym\n"), false);
            break;
        case _ADJUST:
            oled_write_P(PSTR("Adj\n"), false);
            break;
        default:
            oled_write_ln_P(PSTR("-----"), false);
    }
    oled_write_P(PSTR("\n\n"), false);
    // Print Caps Lock status
    led_t led_usb_state = host_keyboard_led_state();
    oled_write_ln_P(PSTR("CAPS"), led_usb_state.caps_lock);
#    ifdef SWAP_HANDS_ENABLE
    // Print current side (left/right)
    oled_write_P(PSTR("\n\n"), false);
    if (swap_hands)
        oled_write_ln_P(PSTR("right"), false);
    else
        oled_write_ln_P(PSTR("left\n"), false);
#    endif
}

oled_rotation_t oled_init_user(oled_rotation_t rotation) { return is_keyboard_master() ? OLED_ROTATION_270 : rotation; }

void oled_task_user(void) {
    if (is_keyboard_master())
        print_status_narrow();
    else
        render_logo();
}

#endif

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case DVORAK:
            if (record->event.pressed) {
                uint8_t keymap = get_highest_layer(eeconfig_read_default_layer()) == _QWERTY ? _DVORAK : _QWERTY;
                set_single_persistent_default_layer(keymap);
            }
            return false;
        case LO_ENT:
            if (record->event.pressed) {
                layer_on(_LOWER);
                update_tri_layer(_LOWER, _RAISE, _SYM);
            } else {
                layer_off(_LOWER);
                update_tri_layer(_LOWER, _RAISE, _SYM);
            }
            return true;
        case RA_SPC:
            if (record->event.pressed) {
                layer_on(_RAISE);
                update_tri_layer(_LOWER, _RAISE, _SYM);
            } else {
                layer_off(_RAISE);
                update_tri_layer(_LOWER, _RAISE, _SYM);
            }
            return true;
    }
    return true;
}

#ifdef ENCODER_ENABLE

bool encoder_update_user(uint8_t index, bool clockwise) {
#    ifdef SWAP_HANDS_ENABLE
    if (index == 0 || index == 1) index ^= swap_hands;
    clockwise ^= swap_hands;
#    endif
    if (index == 0) {
        switch (get_highest_layer(layer_state)) {
            case _LOWER:
            case _RAISE:
                tap_code(clockwise ? KC_BRIU : KC_BRID);
                break;
            default:
                tap_code(clockwise ? KC_VOLU : KC_VOLD);
        }
    } else if (index == 1) {
        switch (get_highest_layer(layer_state)) {
            case _LOWER:
            case _RAISE:
                tap_code(clockwise ? KC_MNXT : KC_MPRV);
                break;
            default:
                tap_code(clockwise ? KC_PGDOWN : KC_PGUP);
        }
    }
    return true;
}

#endif

void debounce_init(uint8_t num_rows) {}

static bool debouncing = false;

void debounce(matrix_row_t raw[], matrix_row_t cooked[], uint8_t num_rows, bool changed) {
    static uint16_t last_change = 0;
    debouncing |= changed;
    if (debouncing && (timer_elapsed(last_change) > DEBOUNCING_DELAY)) {
        for (uint8_t i = 0; i < num_rows; i++) cooked[i] = raw[i];
        debouncing  = false;
        last_change = timer_read();
    }
}

bool debounce_active(void) { return debouncing; }
