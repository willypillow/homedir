# WillyPillow's ANSI 60% Layout #

## Features ##
- Enable link time optimization and disable unused functions for smaller ROM size.
- Enable `EAGER_DEBOUNCING` for lower latency.
- Tweak `MOUSEKEY_*` config for better accuracy.
- NKRO enabled by default.

## Keymap Features ##
- Fn layer inspired by the [KBP V60](https://www.kbparadise.com/v60).
- Dynamic Macros.
- Holding `Space` momentarily switches to Mouse / (Reversed) Number Pad layer.
- `Left Shift` is one-shot.
- Rearrangement of modifiers.
- `Caps Lock` acts as `Backspace`.
- Left `Ctrl` acts as `Esc` when tapped.
- Right modifiers act as arrow keys when tapped.
- `Fn-Enter` toggles tapping behavior, i.e. whether "hold" actions are honored.
- `Fn-Q` toggles between QWERTY and DVORAK.
- `Fn-Z` (F20) & `Fn-X` (F21) can be assigned to OS functions, e.g. brightness control.
- `Fn-E` toggles NKRO.
- `RGB` works as `Backlight Step` if RGB is not enabled, and does nothing if backlight is also not enabled.

## Layout ##
![](kle/layout.png)
