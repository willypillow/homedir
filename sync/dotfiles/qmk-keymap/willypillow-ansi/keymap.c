#include QMK_KEYBOARD_H

#ifdef BACKSLASH_FIX
enum Keycode {
	MY_BSLS = SAFE_RANGE,
};
#else
#define MY_BSLS KC_BSLS
#endif

#define CTL_ESC CTL_T(KC_ESC)
#define DVORAK  TG(_DVORAK)
#define ARROW   TG(_ARROW)
#define RGUI_L  MT(MOD_RGUI, KC_LEFT)
#define RALT_D  MT(MOD_RALT, KC_DOWN)
#define RCTL_R  MT(MOD_RCTL, KC_RIGHT)
#define RSFT_U  MT(MOD_RSFT, KC_UP)
#define OSM_SFT OSM(MOD_LSFT)
#define VOL_M   KC__MUTE
#define VOL_U   KC__VOLUP
#define VOL_D   KC__VOLDOWN
#define PG_BCK  LALT(KC_LEFT)
#define PG_FWD  LALT(KC_RIGHT)
#define FN_ENTR LT(_FN, KC_ENT)
//#define KC_COPY C(KC_INS)
//#define KC_PSTE S(KC_INS)

#if defined(MAGIC_ENABLE) && defined(MAGIC_KEYCODE_ENABLE)
#define NKRO    MAGIC_TOGGLE_NKRO
#else
#define NKRO    _______
#endif

#if defined(RGBLIGHT_ENABLE)
#define RGB     TG(_RGB)
#elif defined(BACKLIGHT_ENABLE)
#define RGB     BL_STEP
#else
#define RGB     KC_TRNS
#endif

enum my_layers {
	_QWERTY = 0,
	_DVORAK,
	_FN,
	_ARROW,
	_MOUSE,
	_RGB,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
	[_QWERTY] = LAYOUT_60_ansi(
		KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSPC,\
		KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC, MY_BSLS,\
		KC_BSPC, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,          KC_ENT, \
		OSM_SFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH,          RSFT_U,          \
		CTL_ESC, KC_LALT, KC_LGUI,                            LT(_MOUSE, KC_SPACE),               FN_ENTR, RGUI_L,  RALT_D,  RCTL_R ),

	[_DVORAK] = LAYOUT_60_ansi(
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_LBRC, KC_RBRC, _______,\
		_______, KC_QUOT, KC_COMM, KC_DOT,  KC_P,    KC_Y,    KC_F,    KC_G,    KC_C,    KC_R,    KC_L,    KC_SLSH, KC_EQL,  _______,\
		_______, KC_A,    KC_O,    KC_E,    KC_U,    KC_I,    KC_D,    KC_H,    KC_T,    KC_N,    KC_S,    KC_MINS,          _______,\
		_______, KC_SCLN, KC_Q,    KC_J,    KC_K,    KC_X,    KC_B,    KC_M,    KC_W,    KC_V,    KC_Z,             _______,         \
		_______, _______, _______,                            _______,                            _______, _______, _______, _______),

	[_FN] = LAYOUT_60_ansi(
		KC_ESC,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_DEL, \
		RGB,     DVORAK,  _______, KC_UP,   KC_TILD, KC_COPY, KC_APP,  KC_PSCR, KC_SLCK, KC_PAUS, KC_UP,   DM_REC1, DM_PLY1, DM_RSTP,\
		KC_CAPS, NKRO,    KC_LEFT, KC_DOWN, KC_RGHT, KC_PSTE, KC_INS,  KC_HOME, KC_PGUP, KC_LEFT, KC_DOWN, KC_RGHT,          ARROW,  \
		KC_LSFT, KC_F20,  KC_F21,  PG_BCK,  PG_FWD,  DM_REC2, DM_PLY2, KC_END,  KC_PGDN, VOL_D,   VOL_U,            VOL_M,           \
		_______, _______, _______,                            _______,                            _______, _______, _______, _______),

	[_ARROW] = LAYOUT_60_ansi(
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,\
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,\
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______,\
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          KC_UP,           \
		_______, _______, _______,                            KC_SPACE,                           _______, KC_LEFT, KC_DOWN, KC_RGHT),

	[_MOUSE] = LAYOUT_60_ansi(
		RESET,   KC_ASDN, KC_ASUP, KC_ASRP, KC_ASTG, _______, _______, KC_NLCK, KC_P0,   _______, KC_PPLS, _______, _______, _______,\
		_______, _______, KC_WH_L, KC_MS_U, KC_WH_R, KC_WH_U, _______, KC_P1,   KC_P2,   KC_P3,   KC_PMNS, _______, _______, _______,\
		_______, _______, KC_MS_L, KC_MS_D, KC_MS_R, KC_WH_D, _______, KC_P4,   KC_P5,   KC_P6,   KC_PAST, _______,          _______,\
		_______, KC_BTN2, KC_BTN3, KC_BTN1, _______, _______, KC_ACL0, KC_P7,   KC_P8,   KC_P9,   KC_PSLS,          _______,         \
		_______, _______, _______,                            _______,                            _______, _______, _______, _______),

#ifdef RGBLIGHT_ENABLE
	[_RGB] = LAYOUT_60_ansi(
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,\
		RGB,     _______, _______, _______, _______, _______, _______, RGB_TOG, RGB_MOD, RGB_HUD, RGB_HUI, _______, _______, _______,\
		_______, _______, _______, _______, _______, _______, _______, RGB_SAD, RGB_SAI, RGB_VAD, RGB_VAI, _______,          _______,\
		BL_STEP, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______,         \
		_______, _______, _______,                            _______,                            _______, _______, _______, _______),
#endif
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
#ifdef BACKSLASH_FIX
	static uint16_t last_bsls = 0;
	switch (keycode) {
		case MY_BSLS:
			if (!record->event.pressed) {
				unregister_code(KC_BSLS);
			} else if (timer_elapsed(last_bsls) > 50) {
				register_code(KC_BSLS);
				last_bsls = timer_read();
			}
			return false;
	}
#endif
	return true;
}

void led_set_user(uint8_t usb_led) {
	if (IS_LED_ON(usb_led, USB_LED_CAPS_LOCK)) {
		DDRE  |=  (1 << PE6);
		PORTE &= ~(1 << PE6);
	} else {
		DDRE  &= ~(1 << PE6);
		PORTE &= ~(1 << PE6);
	}
}

void debounce_init(uint8_t num_rows) { }

static bool debouncing = false;

void debounce(matrix_row_t raw[], matrix_row_t cooked[], uint8_t num_rows, bool changed) {
	static uint16_t last_change = 0;
	debouncing |= changed;
	if (debouncing && (timer_elapsed(last_change) > DEBOUNCING_DELAY)) {
		for (uint8_t i = 0; i < num_rows; i++) cooked[i] = raw[i];
		debouncing = false;
		last_change = timer_read();
	}
}

bool debounce_active(void) {
	return debouncing;
}
